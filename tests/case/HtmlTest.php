<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html;

use Nora;

/**
 * HtmlModuleのテスト
 *
 */
class HtmlTest extends \PHPUnit_Framework_TestCase
{
    public function testSecure ( )
    {
        Nora::module('Html')->configure([
            'charset' => 'utf-8'
        ]);

        $helper = Nora::module('Html')->helper( );

        $tag = $helper->tag('h1', 'タイトル');
        $this->assertEquals('<h1>タイトル</h1>', $tag->render());

        $this->assertEquals('<meta charset="utf-8" />',
            (string) $helper->charset()
        );
        $this->assertEquals('<meta name="viewport" content="width=device-width initial-scale=1.0" />',
            (string) $helper->viewport());

        $this->assertEquals('<meta name="keywords" content="a b \"c" />',
            (string) $helper->keywords('a','b','"c'));
        
        $this->assertEquals('<meta name="description" content="hogehoge" />',
            (string) $helper->description('hogehoge'));

        $this->assertEquals('<title>テスト | タイトル</title>',
            (string) $helper->title('タイトル', 'テスト')->delimiter(' | '));


        echo $helper->opengraphMeta( )
            ->url('hoge.com')
            ->image('イメージ');

        echo $helper->twitterMeta();
        
    }
}
