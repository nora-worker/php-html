<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


class Css extends Tag
{
    private $href;
    private $_path = 'assets/css';

    public function initHelper( )
    {
        $this['rel'] = 'stylesheet';

        return $this;
    }

    public function setPath($path)
    {
        $this->_path = $path;
        return $this;
    }

    public function getTagName( ) {
        return 'link';
    }

    public function isContain( ) {
        return false;
    }

    public function Css($file)
    {
        $this['href'] = $this->genUrl($this->_path."/".$file);
        return $this;
    }

}

