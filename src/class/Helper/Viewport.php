<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


class Viewport extends Meta
{
    public function initHelper( )
    {
        $this['name'] = 'viewport';
        $this['content'] = $this->configure_read('html.viewport', 'width=device-width initial-scale=1.0');
    }

    public function Viewport($viewport = null)
    {
        if ($viewport !== null) $this['viewport'] = $viewport;
        return $this;
    }
}

