<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


class Meta extends Tag
{
    public function initHelper( )
    {
    }

    public function getTagName( ) {
        return 'meta';
    }

    public function isContain()
    {
        return false;
    }

    public function __invoke($text)
    {
        $this['content'] = $text;
        return $this;
    }
}

