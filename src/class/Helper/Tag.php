<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


/**
 * タグ
 */
class Tag extends Base implements \ArrayAccess
{
    private $_tag_name = "";
    private $_inner_text = "";
    private $_attrs = [];

    public function Tag ($tag, $inner = null, $attrs = [])
    {
        $this->_tag_name = $tag;
        $this->_inner_text = $inner;
        $this->_attrs = $attrs;
        return $this;
    }

    public function getTagName( )
    {
        return $this->_tag_name;
    }

    public function getInnerText( )
    {
        return $this->_inner_text;
    }

    public function __toString( )
    {
        return $this->render();
    }

    public function buildAttars( )
    {
        $text = '';
        foreach($this->_attrs as $k=>$v)
        {
            $text.= ' ';
            $text.= $k;
            $text.= "=";
            $v = str_replace('"','\"', $v);
            $text.= "\"$v\"";
        }
        return $text;
    }

    public function isContain( )
    {
        if (!is_null($this->_inner_text))
        {
            return true;
        }
        return false;
    }


    public function render()
    {
        $text = '<';
        $text.= $this->getTagName();
        $text.= $this->buildAttars();

        if ($this->isContain())
        {
            $text.= '>';
            $text.= $this->getInnerText();
            $text.= '</';
            $text.= $this->getTagName();
            $text.= '>';
            return $text;
        }

        $text.= ' />';
        return $text;
    }

    public function offsetGet($name)
    {
        return $this->_attrs[$name];
    }

    public function offsetSet($name, $value)
    {
        return $this->_attrs[$name] = $value;
    }

    public function offsetUnset($name)
    {
        unset($this->_attrs[$name]);
    }

    public function offsetExists($name)
    {
        return isset($this->_attrs[$name]);
    }
}

