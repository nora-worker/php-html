<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


class Title extends Tag
{
    private $_delimiter = ' ';
    private $_titles = [];

    public function Title( )
    {
        $args = func_get_args();
        foreach($args as $title)
        {
            $this->addTitle($title);
        }
        return $this;
    }

    public function getTagName ( )
    {
        return 'title';
    }

    public function getInnerText ( )
    {
        return implode($this->_delimiter, $this->_titles);
    }

    public function isContain( )
    {
        return true;
    }

    public function addTitle($title)
    {
        array_unshift($this->_titles, $title);
        return $this;
    }

    public function delimiter($delimiter)
    {
        $this->_delimiter = $delimiter;
        return $this;
    }
}

