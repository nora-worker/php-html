<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;

use Nora\Module\Html\Helper;
use Nora\Core\Component\Component;

/**
 * ヘルパー
 */
abstract class Base extends Component
{
    public function initComponentImpl( )
    {
        $this->initHelper();
    }

    protected function initHelper()
    {
    }

    public function __toString( )
    {
        return $this->render();
    }

    abstract public function render();

}

