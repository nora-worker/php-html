<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


class HTML5Support extends Base
{
    public function initHelper( )
    {
    }

    public function HTML5Support( )
    {
        return $this;
    }

    public function render( )
    {
        return <<<HTML
<!--
IE support
-->
<!--[if lt IE 9]>
<script src='http://html5shim.googlecode.com/svn/trunk/html5.js'></script>
<![endif]-->
HTML;
    }

}

