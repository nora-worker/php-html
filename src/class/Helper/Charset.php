<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


class Charset extends Meta
{
    public function Charset( )
    {
        $this['charset'] = $this->configure_read('html.charset', 'utf-8');
        return $this;
    }

    public function __invoke($charset = null)
    {
        return $this;
    }
}

