<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


class Description extends Meta
{
    public function initHelper( )
    {
        $this['name'] = 'description';
    }

    public function Description($description = null)
    {
        if ($description !== null)
        {
            $this['content'] = $description;
        }
        return $this;
    }
}

