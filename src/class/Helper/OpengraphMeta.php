<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


class OpengraphMeta extends Base
{
    private $title;
    private $locale;
    private $description;
    private $url;
    private $image;

    public function initHelper( )
    {
        foreach(['title', 'locale', 'description', 'url', 'image'] as $k)
        {
            $this->$k = $this->getScope()->helper()->buildHelper('meta');
            $this->{$k}['content'] = '';
            $this->{$k}['property'] = "og:$k";
        }

        $this->title['content'] = $this
            ->getScope()
            ->helper()
            ->getHtmlHelper('title')
            ->getInnerText();

        $this->description['content'] = $this
            ->getScope()
            ->helper()
            ->getHtmlHelper('description')['content'];
        $this->locale['content'] = $this->getScope()->configure_read('html.locale', 'ja');
    }

    public function render( )
    {
        return 
            $this->title.
            $this->locale.
            $this->description.
            $this->url.
            $this->image;

    }

    public function OpengraphMeta( )
    {
        return $this;
    }

    public function __call($name, $params)
    {
        if (isset($this->$name))
        {
            call_user_func_array($this->$name, $params);
            return $this;
        }

        throw new \RuntimeException(get_class().'::'.$name.' not found');
    }
}

