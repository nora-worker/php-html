<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


class TwitterMeta extends Base
{
    private $title;
    private $locale;
    private $description;
    private $url;
    private $image;

    public function initHelper( )
    {
        foreach(['card', 'site', 'domain', 'creator'] as $k)
        {
            $this->$k = $this->getScope()->helper()->buildHelper('meta');
            $this->{$k}['content'] = '';
            $this->{$k}['name'] = "twitter:$k";
        }

        $this->domain['content'] = $this
            ->getScope()
            ->helper()
            ->getHtmlHelper('title')
            ->getInnerText();

        $this->card['content'] = $this
            ->getScope()
            ->helper()
            ->getHtmlHelper('description')['content'];
    }

    public function render( )
    {
        return 
            $this->card.
            $this->site.
            $this->domain.
            $this->creator;
    }

    public function twitterMeta( )
    {
        return $this;
    }

    public function __call($name, $params)
    {
        if (isset($this->$name))
        {
            call_user_func_array($this->$name, $params);
            return $this;
        }

        throw new \RuntimeException(get_class().'::'.$name.' not found');
    }
}

