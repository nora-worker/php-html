<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html\Helper;


class Keywords extends Meta
{
    private $_keywords = [];

    public function initHelper( )
    {
        $this['name'] = 'keywords';
    }

    public function Keywords( )
    {
        foreach(func_get_args() as $kw)
        {
            $this->addKeyword($kw);
        }

        return $this;
    }

    public function render( )
    {
        $this['content'] = implode(" ", $this->_keywords);
        return parent::render();
    }

    public function addKeyword($word)
    {
        $this->_keywords[] = $word;
        return $this;
    }
}

