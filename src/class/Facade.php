<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html;

use Nora\Core\Module\Module;

/**
 * Htmlモジュール
 */
class Facade extends Module
{
    protected function initModuleImpl( )
    {
        /**
        $this->Configure()->write('html', [
            'charset'  => 'utf-8',
            'viewport' => 'width=device-width initial-scale=1.0',
            'locale'   => 'ja_JP',
        ]);
        **/

        $this->setComponent('@helper', function ( ) {
            $helper = new Helper( );
            $helper->setScope($this->newScope('helper'));
            return $helper;
        });
    }


}
