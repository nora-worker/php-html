<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Html;

use Nora\Core\Component\Componentable;

/**
 * ヘルパー本体
 */
class Helper
{
    use Componentable;

    const CLASS_FORMAT = __namespace__.'\Helper\%s';

    private $_cache = [];

    protected function initComponentImpl( )
    {
    }

    /**
     * ヘルパを取得する
     */
    public function getHtmlHelper($name)
    {
        if (isset($this->_cache[$name]))
        {
            return $this->_cache[$name];
        }
        return $this->_cache[$name] = $this->buildHelper($name);
    }

    /**
     * ヘルパを作成する
     */
    public function buildHelper($name)
    {
        $class = sprintf(self::CLASS_FORMAT, ucfirst($name));
        $helper = new $class( );
        $helper->setScope($this->getScope()->newScope($name));
        return $helper;
    }

    public function isCallable($name)
    {
        $class = sprintf(self::CLASS_FORMAT, ucfirst($name));
        return class_exists($class);
    }

    public function callArray($name, $params)
    {
        return call_user_func_array([$this->getHtmlHelper($name), $name], $params);
    }
}
